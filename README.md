# gitnotes
Notes about how to use git

There are cheat sheets available that contain advanced features, but this intro is intented to provide a quick start to learn git.

---------------------------------------------------------
Quick Start for git

Concept: All git work is local, until published (git push)

Commands:

=== Git Work Flow ===

[Checkout]
git init / git clone

[Change]

[Stage]
git add .

[Commit]
git commit

[Publish to origin]
git push

---------------------------------------------------------
Git Reference

https://git-scm.com/docs

Git Cheat Sheets

https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf
http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf
https://jan-krueger.net/wordpress/wp-content/uploads/2007/09/git-cheat-sheet.pdf

Git Repositories

https://bitbucket.org
https://github.com

---------------------------------------------------------

Quick Start for vi/vim

Remember when learing vi, there are basic concepts, and basic commands.  The concepts, are that vi is a text editor, and that there are two modes.  The main commands to know are to how modify a file, and then either save it, or discard changes.

=== Concepts ===

[Vi Modes]
vi is a text editor, with two basic modes
  *  Command Mode
  *  Insert Mode


=== Commands ===

To edit a text file:

  * [Open a file]
  **   vi filename
  * [Enter insert mode]
  ** i
  * [Make Changes]

  * [Quit, with saving]
  ** [Esc]:wq!
  * [Quit, without saving]
  ** [Esc]:q!


Later, we learn about moving, deleting, copying, searching, save as, opening multiple files, etc.

